/*
 * Copyright (c) 2016.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package com.sampleproject.baseclasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import com.sampleproject.R;
import com.sampleproject.activity.SplashActivity;
import com.sampleproject.component.AppComponent;
import com.sampleproject.utils.NetworkUtil;
import java.io.ByteArrayOutputStream;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public abstract class BaseActivity extends AppCompatActivity {
  private ProgressDialog progressDialog;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (!(this instanceof SplashActivity)) {
      overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }
  }

  @Override public void finish() {
    super.finish();
    if (doAnimationCheck()) {
      overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
  }

  public AppComponent getComponent() {
    return ((BaseApplication) getApplication()).getComponent();
  }

  private boolean doAnimationCheck() {
    //return !(this instanceof SplashActivity) && !(this instanceof HomeActivity) && !(this instanceof LoginActivity);
    return true;
  }

  /**
   * show progress bar
   *
   * @param show show progress when its true
   */
  public void showProgress(boolean show) {
    if (show) {
      if (progressDialog == null) {
        //progressDialog = DialogFactory.createProgressDialog(this, getString(R.string.please_wait));
      }
      progressDialog.show();
    } else {
      if (progressDialog != null) {
        progressDialog.dismiss();
      }
    }
  }
  protected String generateBase64(String path) {
    Bitmap bm = BitmapFactory.decodeFile(path);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
    byte[] b = baos.toByteArray();
    return Base64.encodeToString(b, Base64.DEFAULT);
  }
  @Override protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
  }

  /**
   * check has internet
   *
   * @return has internet
   */
  public boolean hasInternet() {
    return NetworkUtil.isNetworkConnected(this);
  }
}
