package com.sampleproject.component;

import com.sampleproject.networking.ApplicationModule;
import dagger.Component;
import javax.inject.Singleton;

/**
 * @author Harsh
 * @version 1.0
 */
@Singleton @Component(modules = { ApplicationModule.class }) public interface AppComponent {


}
