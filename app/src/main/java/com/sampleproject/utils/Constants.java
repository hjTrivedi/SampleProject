package com.sampleproject.utils;

/**
 * @author Harsh
 * @version 1.0
 */
public interface Constants {
  public static final String BASE_URL = "http://mahendi.swatinfosystem.com/";
  String KEY_TEXT = "key_text";
  String KEY_ID = "key_id";

  String KEY_URL = "key_url";
  String KEY_LIST = "key_list";
}
