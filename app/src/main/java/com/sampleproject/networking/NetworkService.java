package com.sampleproject.networking;

import io.reactivex.Observable;
import java.util.Map;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NetworkService {

  @FormUrlEncoded @POST("api/Login") Observable<String> onLogin(@FieldMap Map<String, String> params);


}
