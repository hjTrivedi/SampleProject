package com.sampleproject.networking;

import com.sampleproject.BuildConfig;
import com.sampleproject.baseclasses.BaseApplication;
import com.sampleproject.utils.Constants;
import dagger.Module;
import dagger.Provides;
import java.io.File;
import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module public class ApplicationModule {
  private File cacheFile;
  private BaseApplication application;

  public ApplicationModule(BaseApplication application) {
    this.application = application;
    cacheFile = new File(application.getCacheDir(), "responses");
  }

  @Provides public OkHttpClient provideLoggingCapableHttpClient() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

    logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

    return new OkHttpClient.Builder().readTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(20, TimeUnit.SECONDS)
        .connectTimeout(20, TimeUnit.SECONDS)
        .addInterceptor(logging)
        .build();
  }

  @Provides @Singleton Retrofit provideCall(OkHttpClient okHttpClient) {
    return new Retrofit.Builder().baseUrl(Constants.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(new ToStringConverterFactory())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build();
  }

  @Provides @Singleton @SuppressWarnings("unused") public NetworkService providesNetworkService(Retrofit retrofit) {
    return retrofit.create(NetworkService.class);
  }

  @Provides @Singleton @SuppressWarnings("unused") public RequestService providesService(NetworkService networkService) {
    return new RequestService(networkService);
  }
}
