package com.sampleproject.networking;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;

public class RequestService {
  private final NetworkService networkService;

  public RequestService(NetworkService networkService) {
    this.networkService = networkService;
  }


  public Disposable onLogin(HashMap<String, String> params, final GetCallback<String> callback) {

    return networkService.onLogin(params).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends String>>() {
          @Override public ObservableSource<? extends String> apply(@NonNull Throwable throwable) throws Exception {
            return Observable.error(throwable);
          }
        }).subscribeWith(new DisposableObserver<String>() {
          @Override public void onNext(@NonNull String s) {
            callback.onSuccess(s);
          }

          @Override public void onError(@NonNull Throwable e) {
            callback.onError(new NetworkError(e));
          }

          @Override public void onComplete() {

          }
        });
  }

  public interface GetCallback<T> {
    void onSuccess(T response);

    void onError(NetworkError networkError);
  }
}
